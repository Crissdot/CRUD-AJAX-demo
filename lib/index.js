// Control DOM
import { Todo } from './Todo.js';

window.addEventListener("load", (ev) => {
    let container = document.querySelector("#root ul");

    document.querySelector("#mainForm").addEventListener("submit",(ev)=>{
        ev.preventDefault();
        const form = ev.target;

        const textarea = form.querySelector("textarea");

        const button = form.querySelector("[type='submit']");
        button.disabled = true;

        let todo = new Todo({title: textarea.value});

        todo.save().then(()=>{
            textarea.value = "";
            button.disabled = false;

            let li = buildDOMElement(todo);
            container.prepend(li);

        })

        return false;
    })

    Todo.all().then(todos =>{
        todos.forEach(todo => {
            let node = buildDOMElement(todo);

            container.prepend(node);
        });
    });

    let buildDOMElement = (todo) =>{
        let li = document.createElement("li");
        li.innerHTML = `
            <h1>${todo.title}</h1>
            <button class="close">X</button>
        `;

        li.querySelector(".close").addEventListener("click",(ev)=>{
            todo.destroy();
            li.remove();
        })

        editInPlace(li.querySelector("h1"),todo)

        return li
    }

    let editInPlace = (node, todo) => {
        node.addEventListener("click",(ev)=>{
            let inputText = document.createElement("textarea");
            let nodeText = node.innerHTML;
            inputText.value = nodeText;
            node.replaceWith(inputText);
            inputText.focus();


            inputText.addEventListener("focusout",(ev)=>{
                inputText.replaceWith(node);
                todo.title = inputText.value;

                node.innerHTML = todo.title;

                if(todo.title != nodeText) todo.save().then(r => console.log(r));
            });

        })
    }
});